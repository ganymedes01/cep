'use strict';

const cors = require('cors');
const async = require('async');
const { db } = require('./libs');
const morgan = require('morgan');
const express = require('express');

const app = express();

exports.init = callback =>
  async.parallel([callback => db.query('SELECT 1;', callback)], err => {
    if (err) return callback(err);

    // Apenas adiciona o logger se não estivermos em ambiente de teste
    if (process.env.NODE_ENV !== 'test') app.use(morgan('[:date[web]] [:response-time ms] [:status] :method :url'));

    // Desabilita headers x-powered-by e etag
    app.disable('x-powered-by');
    app.disable('etag');

    // Cors
    app.use(cors());

    // Cria as Rotas
    app.use('/api/', require('./routes'));

    app.all('*', (req, res) => res.status(404).send({ error: 'not found' }));

    // Start do Servidor
    app.listen(process.env.HTTP_PORT || 80, callback);
  });
