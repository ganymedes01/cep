'use strict';

require('dotenv').config();

const { http } = require('../libs');
const app = require('../app');
const { expect, should } = require('chai');

const url = `http://localhost:${process.env.HTTP_PORT}`;

describe('auth', () => {
  before(done => {
    app.init(err => {
      should().not.exist(err);

      done();
    });
  });

  it('Method: GET', done => {
    http.get(`${url}/api/30140081`, (err, res) => {
      should().not.exist(err);

      expect(JSON.parse(res)).to.be.deep.equal({
        cep: '30140081',
        logradouro: 'Rua Bernardo Guimarães',
        complemento: 'de 701/702 a 949/950',
        bairro: 'Savassi',
        cidade: 'Belo Horizonte',
        uf: 'MG'
      });
      done();
    });
  });

  it('Method: GET (not found)', done => {
    http.get(`${url}/api/88383833`, err => {
      expect(JSON.parse(err)).to.be.deep.equal({ error: 'not found' });
      done();
    });
  });

  it('Method: GET (invalid)', done => {
    http.get(`${url}/api/abcgdhs`, err => {
      expect(JSON.parse(err)).to.be.deep.equal({ error: 'invalid cep' });
      done();
    });
  });
});
