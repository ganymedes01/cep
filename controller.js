'use strict';

const cheerio = require('cheerio');
const windows1252 = require('windows-1252');
const { sql, db, logger, http } = require('./libs');

const URL = 'http://www.buscacep.correios.com.br/sistemas/buscacep/resultadoBuscaCepEndereco.cfm';

const selectors = {
  INPUT_CEP: '#Geral > div > div > span:nth-child(3) > label > input',
  SUBMIT: '#Geral > div > div > div.btnform > input',
  RESULT: 'body > div.back > div.tabs > div:nth-child(2) > div > div > div.column2 > div.content > div.ctrlcontent > p',
  TABLE_STREET:
    'body > div > div.tabs > div:nth-child(2) > div > div > div.column2 > div.content > div.ctrlcontent > table > tbody > tr:nth-child(2) > td:nth-child(1)',
  TABLE_NEIGHBOURHOOD:
    'body > div > div.tabs > div:nth-child(2) > div > div > div.column2 > div.content > div.ctrlcontent > table > tbody > tr:nth-child(2) > td:nth-child(2)',
  TABLE_CITY:
    'body > div > div.tabs > div:nth-child(2) > div > div > div.column2 > div.content > div.ctrlcontent > table > tbody > tr:nth-child(2) > td:nth-child(3)',
  TABLE_CEP:
    'body > div > div.tabs > div:nth-child(2) > div > div > div.column2 > div.content > div.ctrlcontent > table > tbody > tr:nth-child(2) > td:nth-child(4)'
};

exports.get = (req, res) => {
  const { cep } = req.params;
  const maxCheckedDate = new Date(Date.now() - 7 * 24 * 3600 * 1000); // Uma semana

  // Error check
  if (!cep || typeof cep !== 'string' || !cep.match(/^([0-9]{8})$/))
    return res.status(400).send({ error: 'invalid cep' });

  // Busca na tabela de CEPs inválidos
  db.query({ text: sql.selectInvalid, values: [cep, maxCheckedDate] }, (err, response) => {
    if (err) return res.status(500).send(err.message);

    // Caso o CEP esteja na tabela de CEPs inválidos, retorna 404
    if (response.rows[0]) return res.status(404).send({ error: 'not found' });

    // Busca na tabela de CEPs
    db.query({ text: sql.select, values: [cep, maxCheckedDate] }, (err, response) => {
      if (err) return res.status(500).send(err.message);

      // Caso o CEP seja encontrado, retorna ele
      if (response.rows[0]) return res.status(200).send(fixReturn(response.rows[0]));

      // Caso o CEP não tenha sido encontrado, busca no site dos correios
      getAndStore(cep)
        .then(([status, response]) => res.status(status).send(response))
        .catch(([status, error]) => res.status(status).send({ error }));
    });
  });
};

const fixReturn = res => {
  res.cep = res.id;
  delete res.id;
  delete res.checked_at;
  return res;
};

const getAndStore = async cep => {
  try {
    const res = await getFromCorreios(cep);
    const values = [cep, res.logradouro, res.complemento, res.bairro, res.cidade, res.uf];
    const ret = await db.query({ text: sql.insert, values });

    return [200, { cep, ...ret.rows[0] }];
  } catch (err) {
    if (err.message === 'dados nao encontrados') {
      await db.query({ text: sql.invalidate, values: [cep] });
      return [404, 'not found'];
    }

    logger.error(err);
    return [500, err.message];
  }
};

const getFromCorreios = async cep => {
  const body = `relaxation=${cep}&tipoCEP=ALL&semelhante=N`;
  const html = await http.request(URL, body);

  const $ = cheerio.load(windows1252.decode(html.toString('binary')));
  const res = $(selectors.RESULT).text();

  if (res === 'DADOS ENCONTRADOS COM SUCESSO.') {
    const street = $(selectors.TABLE_STREET)
      .text()
      .split(' - ');
    const bairro = $(selectors.TABLE_NEIGHBOURHOOD)
      .text()
      .trim();
    const city = $(selectors.TABLE_CITY)
      .text()
      .split('/');
    const cep = $(selectors.TABLE_CEP)
      .text()
      .trim();

    return {
      logradouro: street[0].trim(),
      complemento: street[1] ? street[1].trim() : null,
      bairro,
      cidade: city[0].trim(),
      uf: city[1].trim().toUpperCase(),
      cep
    };
  }

  throw new Error(res.toLowerCase());
};
