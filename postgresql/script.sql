 CREATE TABLE "cep" (
         "id" VARCHAR(8) PRIMARY KEY CHECK ("id" ~ '^([0-9]{8})$'::TEXT),
 "logradouro" VARCHAR NOT NULL,
"complemento" VARCHAR,
     "bairro" VARCHAR NOT NULL,
     "cidade" VARCHAR NOT NULL,
         "uf" VARCHAR NOT NULL CHECK (LENGTH("uf") = 2),
 "checked_at" TIMESTAMP NOT NULL DEFAULT now()
);

CREATE TABLE "invalid" (
        "id" VARCHAR(8) PRIMARY KEY CHECK ("id" ~ '^([0-9]{8})$'::TEXT),
"checked_at" TIMESTAMP NOT NULL DEFAULT now()
);
