'use strict';

const fs = require('fs');
const http = require('http');
const path = require('path');
const { Pool } = require('pg');

class File {
  constructor(folder) {
    Object.assign(this, this.load(folder));
  }

  load(folder) {
    return fs.readdirSync(folder).reduce((acc, res) => {
      const dirInner = path.resolve(folder, res);

      if (fs.statSync(dirInner).isDirectory()) {
        const files = this.load(dirInner);
        if (Object.keys(files).length > 0) acc[res] = files;
      } else if (res.endsWith('.sql')) {
        const key = this.camelCase(res.substring(0, res.length - 4));
        acc[key] = fs.readFileSync(path.join(folder, res), 'utf8');
      }

      return acc;
    }, {});
  }

  camelCase(input) {
    return input.toLowerCase().replace(/_(.)/g, (match, group1) => group1.toUpperCase());
  }
}

const request = (url, body) => {
  return new Promise((resolve, reject) => {
    const req = http.request(
      url,
      { method: 'POST', headers: { 'content-type': 'application/x-www-form-urlencoded' } },
      res => {
        const chunks = [];
        res.on('data', chunk => chunks.push(chunk));
        res.on('end', () => resolve(Buffer.concat(chunks)));
      }
    );

    req.on('error', err => reject(err));
    req.write(body);
    req.end();
  });
};

const get = (url, callback) => {
  http
    .get(url, res => {
      const { statusCode } = res;

      const chunks = [];
      res.on('data', chunk => chunks.push(chunk));
      res.on('end', () => {
        const data = Buffer.concat(chunks).toString('utf8');
        if (statusCode === 200) callback(null, data);
        else callback(data);
      });
    })
    .on('error', err => callback(err));
};

module.exports = {
  sql: new File(path.join(__dirname, 'sql')),
  db: new Pool(),
  logger: {
    error: console.error, // eslint-disable-line no-console
    info: console.info // eslint-disable-line no-console
  },
  http: {
    request,
    get
  }
};
