FROM node:alpine

ENV HOME=/app

EXPOSE 80

WORKDIR $HOME
COPY ./ $HOME

RUN yarn --prod

CMD [ "yarn", "start" ]
