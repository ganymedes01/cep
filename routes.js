'use strict';

const express = require('express');
const controller = require('./controller');

const router = express.Router();

router.all('/ping', (req, res) => res.send({ ping: 'pong' }));

router.get('/:cep', controller.get);

module.exports = router;
