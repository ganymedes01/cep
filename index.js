'use strict';

if (process.env.NODE_ENV !== 'production') require('dotenv').config();

const { logger } = require('./libs');

const app = require('./app');
app.init(err => {
  if (err) throw err;

  logger.info(`Ready! (${process.env.NODE_ENV})`);
});
