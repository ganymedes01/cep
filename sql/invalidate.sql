INSERT INTO "invalid" ("id")
     VALUES ($1::VARCHAR)
         ON CONFLICT ("id")
  DO UPDATE SET "checked_at" = now();
