SELECT 1
  FROM "invalid"
 WHERE "id" = $1::VARCHAR
   AND "checked_at" > $2::TIMESTAMP;
