SELECT *
  FROM "cep"
 WHERE "id" = $1::VARCHAR
   AND "checked_at" > $2::TIMESTAMP;
