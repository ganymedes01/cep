INSERT INTO "cep"
            ("id",
            "logradouro",
            "complemento",
            "bairro",
            "cidade",
            "uf")
     VALUES ($1::VARCHAR,
             $2::VARCHAR,
             $3::VARCHAR,
             $4::VARCHAR,
             $5::VARCHAR,
             $6::CHAR(2))
          ON CONFLICT ("id")
   DO UPDATE SET "logradouro" = $2,
                 "complemento" = $3,
                 "bairro" = $4,
                 "cidade" = $5,
                 "uf" = $6,
                 "checked_at" = now()
   RETURNING "logradouro",
             "complemento",
             "bairro",
             "cidade",
             "uf";
